import React from "react";

function Home() {
  return (
<div className="home">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/1076/articles/2018/01/gettyimages-cooper-test-1517255281.jpg?crop=1xw:0.786xh;center,top&resize=768:*"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Cooper Tracker</h1>
            <p>
              In this application you can track your students cooper results. Frirst add students and then records. If you want to delete student, delete record first.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;