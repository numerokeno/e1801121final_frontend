export { default as Navigation } from "./Navigation";
export { default as Home } from "./Home";
export { default as Students } from "./Students";
export { default as Result } from "./Result";