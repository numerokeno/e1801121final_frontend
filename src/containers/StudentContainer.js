import React, { useEffect, useState } from 'react';
import StudentComponent from "../components/StudentComponent";
import StudentList from '../components/StudentList';
import{deleteStudent, getStudents, saveStudent} from '../controllers/StudentController';

const StudentController = () =>{
    const[students, setStudents] = useState([]);
    const [student, setStudent] = useState({"name":"","classname":""});


    useEffect(()=>{
        initData();
    },[]);
    
    async function initData(){
        //pyydetään students controllerilta
        getStudents() 
        .then(data=>{
            setStudents(data);          //tallennetaan tulos paikallisesti
        })
        .catch((error) => {
            alert('Error:', error);
        });  
    }
    
    //turha

    //takes care updating the observation
    const handleChange = (e) => {
        let studentNew = {...student};   //avoid mutation
        studentNew[e.target.name]=e.target.value;
        setStudent(studentNew); //storing to the memory of container
      };

    const handleSubmit= async()=>{
        const result = await saveStudent(student);
        setStudent({"name":"","classname":""});
        let newStudents = [...students];  
        newStudents.push(result)      
        setStudents(newStudents);
    }

    const deleteSubmit= async(id) =>{
        const result = await deleteStudent(id);
        console.log(id);
        let newStudents = [...students].filter(i => i.id !== id);      
        setStudents(newStudents);
    }
    



    return(
        <div>
            <StudentComponent submit={handleSubmit} student={student} change={handleChange}/>
            <StudentList data={students} submit={deleteSubmit}></StudentList>
        </div>
    );
}
export default StudentController;