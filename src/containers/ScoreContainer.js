import React, { useEffect, useState } from 'react';
import ScoreComponent from '../components/ScoreComponent';
import StudentComponent from "../components/ScoreComponent";
import ScoreList from '../components/ScoreList';
import StudentList from '../components/StudentList';
import{deleteScore, getScores, saveScore, getStudents} from '../controllers/ScoreController';

const ScoreContainer = () => {
    const [students, setStudents] = useState([]); //constin perässä ensin muuttuja sitten funktio
    //const [student, setStudent] = useState({}); //constin perässä ensin muuttuja sitten funktio
    const [scores, setScores] = useState([]); 
    const [score, setScore] = useState({});

    useEffect(()=>{
        initData();
    },[]);

    async function initData(){
        getStudents() 
        .then(data=>{
            setStudents(data);        
        })
        .catch((error) => {
            alert('Error:', error);
        });
        
        getScores() 
        .then(data=>{
            setScores(data);         
        })
        .catch((error) => {
            alert('Error:', error);
        });
    }
   
    const handleChange = (e) => {
        let scoreNew = {...score};   //avoid mutation
        scoreNew[e.target.name]=e.target.value;
        //console.log("test: "+scoreNew[e.target.name]);
        setScore(scoreNew); //storing to the memory of container
        //console.log(scoreNew);
      };

    const handleSubmit= async()=>{
        const foundStudent = students.find(i=>i.id === Number(score.studentid));
        if(!foundStudent){
            return
        }

        const updatetScore = {
            rundate:score.rundate,
            rundistance:score.rundistance,
            supervisor:score.supervisor,
            student:foundStudent
        }
        const result = await saveScore(updatetScore);
    
        //setScore({"id":"","student":{"id":"","name":"","classname":""},"rundistance":"","runtime":"","supervisor":""});
        let newScore = [...scores];  
        newScore.push(result)      
        setScores(newScore);
        console.log(newScore);
    }
    

    const deleteSubmit= async(id) =>{
        const result = await deleteScore(id);
        console.log(id);
        let newScores = [...scores].filter(i => i.id !== id);      
        setScores(newScores);
    }
    return(
        <div>
            {/* Kutsutaan controllerin getBirds -funtiota ja välitetään

            <ScoreList data={scores} submit={deleteSubmit}></ScoreList>

            linnut componetille Ctrl+Shift+/*/}
            <ScoreComponent theStudents={students} submit={handleSubmit} score={score} change={handleChange}/>
            <ScoreList data={scores} submit={deleteSubmit}></ScoreList>
            
       </div>
    );
}

export default ScoreContainer;