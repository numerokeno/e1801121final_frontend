import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Navigation, Home, Students, Result } from "./pages";
//import {Navigation} from './components/Navigation';

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={() => <Home />} />
          <Route path="/students" exact component={() => <Students />} />
          <Route path="/result" exact component={() => <Result />} />
        </Switch>
      </Router>
        
    </div>
  );
}

export default App;
