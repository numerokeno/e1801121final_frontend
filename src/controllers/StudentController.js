export async function getStudents() {
    return fetch("http://localhost:8080/students")
        .then(response => response.json())
        .then(data => {
            return (data)
        });
}
//Funktio tallentaa studentin tauluun
export async function saveStudent(student) {
    const response = await fetch("http://localhost:8080/student", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
    });
    return response.json();
}
//DELETE
export async function deleteStudent(id) {
    return await fetch("http://localhost:8080/student/"+ id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
    });
}

