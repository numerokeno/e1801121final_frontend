//haetaan studentit
export async function getStudents() {
    return fetch("http://localhost:8080/students")
        .then(response => response.json())
        .then(data => {
            return (data)
        });
}
//Haetaan resultit
export async function getScores() {
    return fetch("http://localhost:8080/scores")
        .then(response => response.json())
        .then(data => {
            return (data)
        });
}
//Funktio tallentaa studentin tauluun
export async function saveScore(score) {
    const response = await fetch("http://localhost:8080/score", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(score)
    });
    return response.json();
}
//DELETE
export async function deleteScore(id) {
    return await fetch("http://localhost:8080/score/" + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
    });
}

