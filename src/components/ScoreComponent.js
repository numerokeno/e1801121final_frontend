import React from 'react';

const ScoreComponent = (props) =>{
    const studentOption =props.theStudents.map((student, index)=>{
        return <option key={parseInt(index)} value={student.id}>{student.name}</option>
    })

    return(
        <div>
            <br/>
            <h2>Add new Cooper result!</h2> <br/>
            <table style={{marginLeft: '45%'}}>
            <tr><td>Student</td><td><select onChange={e=>props.change(e)} name="studentid">{studentOption}</select></td></tr>
            <tr><td>Run Date</td><td><input onChange={e=>props.change(e)} name="rundate" value={props.score.rundate} type="date"></input></td></tr>
            <tr><td>Run Distance</td><td><input onChange={e=>props.change(e)} name="rundistance" value={props.score.rundistance} type="text"></input></td></tr>
            <tr><td>Teacher</td><td><input onChange={e=>props.change(e)} name="supervisor" value={props.score.supervisor} type="text"></input></td></tr>
            <tr><td><button onClick={props.submit}>Save</button></td></tr>
            </table>
        </div>
    );
}
export default ScoreComponent;