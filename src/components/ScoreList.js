import React from 'react';
//npm install react-table-v6
import ReactTable from "react-table-v6";
import 'react-table-v6/react-table.css';

const StudentList = (props) => {
    const columns = [{
        Header: 'Score ID',
        accessor: 'id' // String-based value accessors!
    }, {
        Header: 'Run Date',
        accessor: 'rundate',
    }, {
        Header: 'Run Distance/metric',
        accessor: 'rundistance',
    },{
        Header: 'Student ID',
        accessor: 'student.id',
    },{
        Header: 'Student Name',
        accessor: 'student.name',
    },{
        Header: 'Student Class',
        accessor: 'student.classname',
    },{
        Header: 'Supervisor Teacher',
        accessor: 'supervisor',
    },{
        Header: 'Delete',
        accessor: "name",
        Cell: (cell) => (
            <button onClick={ ()=> (props.submit(cell.original.id))}>DELETE</button>
        )
    }];


    return (
        <div>
            <ReactTable
                data={props.data}
                columns={columns}
            />
        </div>
    )
}

export default StudentList;