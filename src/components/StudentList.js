import React from 'react';
//npm install react-table-v6
import ReactTable from "react-table-v6";
import 'react-table-v6/react-table.css';

const StudentList = (props) => {
    const columns = [{
        Header: 'Student ID',
        accessor: 'id' // String-based value accessors!
    }, {
        Header: 'Student Name',
        accessor: 'name',
    }, {
        Header: 'Class',
        accessor: 'classname',
    }, {
        Header: 'Delete',
        accessor: "delete",
        Cell: (cell) => (
            <button onClick={ ()=> (props.submit(cell.original.id))}>DELETE</button>
        )
    }];


    return (
        <div>
            <ReactTable
                data={props.data}
                columns={columns}
            />
        </div>
    )
}

export default StudentList;