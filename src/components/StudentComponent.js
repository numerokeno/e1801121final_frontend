import React from 'react';

const StudentComponent = (props) =>{
    return(
        <div>
            <br/>
            <h2>Add new Sudent</h2> <br/>
             <table style={{marginLeft: '43%'}}>
                <tr><td>Student name</td><td><input onChange={e=>props.change(e)} name="name" value={props.student.name} type="text"></input></td></tr>
                <tr><td>Student Class</td><td><input onChange={e=>props.change(e)} name="classname" value={props.student.classname} type="text"></input></td></tr>
                <tr><td><button onClick={props.submit}>Save</button></td></tr> <br/> <br/>
            </table>
        </div>
    );
}
export default StudentComponent;